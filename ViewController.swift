//
//  ViewController.swift
//  SeeUMart
//
//  Created by Yau Khoo on 29/01/2018.
//  Copyright © 2018 seeumart. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    var webview: WKWebView!
    var timeOut: Timer!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var logoImageView: UIImageView!
//    var lastVisitedUrl: String = "http://wap.seeumart.my/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImageView.layer.zPosition = 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Network.reachability?.isConnectedToNetwork)! {
            initWebview()
        } else {
            showWifiAlert(title: "The internet is not available", message: "Please try again")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController {
    func initWebview() {
        let webConfiguration = WKWebViewConfiguration()
        let preference = WKPreferences()
        preference.javaScriptEnabled = true

        webview = WKWebView(frame:.zero , configuration: webConfiguration)
        webview.alpha = 0
        webview.navigationDelegate = self
        webview.uiDelegate = self
        webview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webview)
        view.addSubview(activityIndicator)
        activityIndicator.layer.zPosition = 1
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":webview]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":webview]))
        
        self.activityIndicator.hidesWhenStopped = true
        
        let refreshControl = UIRefreshControl()
        refreshControl.bounds = CGRect(x: refreshControl.bounds.origin.x, y: 50, width: refreshControl.bounds.width, height: refreshControl.bounds.height)
        refreshControl.addTarget(self, action: #selector(refreshWebView), for: UIControlEvents.valueChanged)
        
        webview.scrollView.addSubview(refreshControl)
        
        let url = URL(string: "http://wap.seeumart.my")!
        webview.load(URLRequest(url: url))
        webview.allowsBackForwardNavigationGestures = true
    }
    
    @objc func refreshWebView(sender: UIRefreshControl) {
        
//        if self.webview.url?.absoluteString.lowercased().range(of:"404.html") != nil {
//            // 404
//            let url = URL(string: self.lastVisitedUrl)
//            self.webview.load(URLRequest(url: url!))
//            sender.endRefreshing()
//        } else if self.webview.url?.absoluteString.lowercased().range(of:"no_internet.html") != nil {
//            // no internet
//            let url = URL(string: self.lastVisitedUrl)
//            self.webview.load(URLRequest(url: url!))
//            sender.endRefreshing()
//        } else if self.webview.url?.absoluteString.lowercased().range(of:"slow_internet.html") != nil {
//            // slow internet
//            let url = URL(string: self.lastVisitedUrl)
//            self.webview.load(URLRequest(url: url!))
//            sender.endRefreshing()
//        } else {
            print("refresh")
            webview.reload()
            sender.endRefreshing()
//        }
    }
}

extension ViewController {
    func fileURLForBuggyWKWebView8(fileURL: URL) throws -> URL {
        // Some safety checks
        if !fileURL.isFileURL {
            throw NSError(
                domain: "BuggyWKWebViewDomain",
                code: 1001,
                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("URL must be a file URL.", comment:"")])
        }
        try! fileURL.checkResourceIsReachable()
        
        // Create "/temp/www" directory
        let fm = FileManager.default
        let tmpDirURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("www")
        try! fm.createDirectory(at: tmpDirURL, withIntermediateDirectories: true, attributes: nil)
        
        // Now copy given file to the temp directory
        let dstURL = tmpDirURL.appendingPathComponent(fileURL.lastPathComponent)
        let _ = try? fm.removeItem(at: dstURL)
        try! fm.copyItem(at: fileURL, to: dstURL)
        
        // Files in "/temp/www" load flawlesly :)
        return dstURL
    }
}

extension ViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        print(error._code)
        self.activityIndicator.stopAnimating()
//        self.timeOut.invalidate()

        if error._code == -1001 { // TIMED OUT:
            self.showAlert(title: "The internet is not available", message: "Please try again")
        } else if error._code == -1003 { // SERVER CANNOT BE FOUND
            self.showAlert(title: "The server is having trouble", message: "Please try again")
        } else if error._code == -1100 { // URL NOT FOUND ON SERVER
            self.showAlert(title: "The server is having trouble", message: "Please try again")
        } else if error._code == -1009 { // NO INTERNET
            self.showAlert(title: "The internet is not available", message: "Please try again")
        } else {
            self.showHomeAlert(title: "Something went wrong", message: "Please try again")
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
        self.activityIndicator.startAnimating()
//        self.timeOut = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(Web), userInfo: nil, repeats: false)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finish to load")
        UIView.animate(withDuration: 0.5, animations: {
            self.logoImageView.alpha = 0
            self.webview.alpha = 1
        }) { (finished) in
            self.logoImageView.isHidden = true
        }
        self.activityIndicator.stopAnimating()
//        self.timeOut.invalidate()
        
        if self.webview.isHidden {
            self.webview.isHidden = false
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url,
                let host = url.host, (host.hasPrefix("www.twitter.com") || host.hasPrefix("twitter.com")), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:])
                        }
                    } else {
                        // for versions below iOS 10
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }
                    }

                    print(url)
                    decisionHandler(.cancel)
            } else if let url = navigationAction.request.url,
                let host = url.host, (host.hasPrefix("www.instagram.com") || host.hasPrefix("instagram.com")), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:])
                        }
                    } else {
                        // for versions below iOS 10
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }
                    }

                    print(url)
                    decisionHandler(.cancel)
            } else if let url = navigationAction.request.url,
                let host = url.host, (host.hasPrefix("www.facebook.com") || host.hasPrefix("facebook.com")), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:])
                    }
                } else {
                    // for versions below iOS 10
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }
                
                print(url)
                decisionHandler(.cancel)
            } else if let url = navigationAction.request.url,
                let host = url.host, !host.hasPrefix("wap.seeumart.my") {
                decisionHandler(.cancel)
                
                self.webview.load(URLRequest(url: url))
            } else {
                print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
    
    @objc func cancelWeb() {
        print("cancelweb")
        self.showAlert(title: "The internet is not available", message: "Please try again")
    }
    
    func showAlert(title: String, message: String) {
        self.webview.isHidden = true
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.webview.reload()
        }));
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHomeAlert(title: String, message: String) {
        self.webview.isHidden = true
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.webview.load(URLRequest(url: URL(string: "http://wap.seeumart.my")!))
        }));
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showWifiAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if (Network.reachability?.isConnectedToNetwork)! {
                self.initWebview()
            } else {
                self.showWifiAlert(title: "The internet is not available", message: "Please try again")
            }
        }));
        
        alert.addAction(UIAlertAction(title: "Exit", style: .destructive, handler: { action in
            exit(0);
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
